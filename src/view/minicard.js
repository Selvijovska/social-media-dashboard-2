import {insertInFirstList} from "../base.js"

export function minicard([dr, data]) {
    const markup2 = data.daily_report.map(report => {
        return  `
        <li class="small-card">
                    <div class="page-views">
                        <p>${report.data_info}</p>
                        <img src="images/${dr}.svg" alt="${dr}" />
                    </div>
                    <div class="numbers-wrapper">
                        <p class="counter" data-target="${report.data_result}">0</p>
                        <div class="percentage-wrapper">
                            <img src="${report.percentage > 0 ? "images/up.svg" : "images/down.svg"}" alt="">
                            <h6 class="${report.percentage > 0 ? "increasing" : "decreasing"}">${report.percentage}%</h6>
                        </div>
                    </div>
                </li>
        `
    }).join('')

    insertInFirstList.insertAdjacentHTML('beforeend',markup2)
};

