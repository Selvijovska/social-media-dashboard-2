import {insertInSecondList} from "../base.js"

export function card ([sm, data]) {
    const markup = `
        <li class="card ${sm}">
            <div class="curved-border-${sm}"></div>
                <div class="social-media">
                    <img src="images/${sm}.svg" alt="facebook" />
                    <p>${data.user}</p>
                </div>
                <div class="followers">
                    <h2 class="counter" data-target="${data.followers}">0</h2>
                    <p>${data.followers_type}</p>
                </div>
                <div class="overview">
                    <img src="${data.new_followers > 0 ? "images/up.svg" : "images/down.svg"}" />
                    <p class="${data.new_followers > 0 ? "increasing" : "decreasing"}">${data.new_followers} Today</p>
                </div>
        </li>
    `

    insertInSecondList.insertAdjacentHTML('beforeend', markup);
   
};

