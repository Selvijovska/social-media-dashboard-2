import {card} from "./view/card.js";
import {minicard} from "./view/minicard.js";
import {checkbox, speed} from "./base.js"


function attachEventListeners () {
    checkbox.addEventListener('change', function() {
        if (this.checked) {
            document.documentElement.setAttribute('data-theme', 'light')       
        }else {
            document.documentElement.setAttribute('data-theme', 'dark')
        }
    });
}

function kFormatter (num) {
    if (num > 10999) {
        return Math.trunc(num/1000) + 'k';
    } else {
        return num;
    }
}

function numCounter () {
    const counters = document.querySelectorAll('.counter');

    counters.forEach( counter => {
        const updateCount = () => {
            const target = +counter.getAttribute('data-target');
            const count = +counter.innerText; 
    
            const inc = target / speed;
    
            if (count < target) {
                counter.innerText = Math.ceil(count + inc); 
                kFormatter(target);
                setTimeout(updateCount, 1); 
            } else {
                counter.innerText = kFormatter(target);
            }
        }    
        updateCount();
    });
}

function fetchData () {
    return fetch('../data.json')
        .then(res => res.json())
}

// IIFE - Immediatelly Invoked Function Expression
(async () => {
    attachEventListeners();

    const data = await fetchData();

    Object.entries(data).map(item => card(item))

    // card(['facebook', data.facebook]);

    Object.entries(data).map(item => minicard(item))

    numCounter();
})();